import cryptonator
from taskapp.celery import app
from django.db import connection, transaction
import logging
from apps.currencies.models import (
    CurrencyAbbreviation,
    Currency,
)

logger = logging.getLogger(__name__)


def create_initial_currency():
    try:
        instance = Currency.objects.create(
            first_currency='BTC',
            second_currency='USD',
        )
    except Exception as e:
        logger.error(str(e))
        pass
    else:
        course = cryptonator.get_exchange_rate(instance.first_currency, instance.second_currency)
        instance.course = course
        instance.save()


def upload_abbreviations():
    if not CurrencyAbbreviation.objects.exists():
        abr_list = cryptonator.get_available_currencies()
        CurrencyAbbreviation.objects.create(abbreviations=abr_list)
    if not Currency.objects.exists():
        create_initial_currency()


@app.task(bind=True)
def currencies_fetcher(self, *args, **kwargs):
    with transaction.atomic():
        currency_list = Currency.objects.all()
        if currency_list.exists():
            for currency_pair in currency_list:
                course = cryptonator.get_exchange_rate(currency_pair.first_currency, currency_pair.second_currency)
                currency_pair.course = course
                currency_pair.save()
        else:
            create_initial_currency()
