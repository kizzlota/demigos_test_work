from rest_framework import serializers
from apps.currencies.models import (
    Currency,
    CurrencyAbbreviation,
)
from django.db import IntegrityError
from currencies.utils import fetch_course
import cryptonator


class CurrencySimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = (
            'id',
            'first_currency',
            'second_currency',
            'course',
        )

    def validate(self, attrs):
        first_currency = attrs.get('first_currency', None)
        second_currency = attrs.get('second_currency', None)
        if first_currency == second_currency:
            raise serializers.ValidationError({"details": "Does not permitted to use same pairs"})
        if first_currency and second_currency:
            if any([first_currency.islower(), second_currency.islower()]):
                attrs.update({"first_currency": first_currency.upper(), "second_currency": second_currency.upper()})
                return attrs
            elif all([first_currency.isupper(), second_currency.isupper()]):
                return attrs

    def create(self, validated_data):
        first_currency = validated_data.get('first_currency', None)
        second_currency = validated_data.get('second_currency', None)
        available_currencies_list = CurrencyAbbreviation.objects.values_list('abbreviations', flat=True).first()
        matched = set(validated_data.values()) == set(validated_data.values()) & set(available_currencies_list)
        if matched:
            try:
                validated_data['course'] = fetch_course(first_currency=first_currency, second_currency=second_currency)
                instance = Currency.objects.create(**validated_data)
            except IntegrityError:
                raise serializers.ValidationError(
                    f'Currency pair {first_currency} to {second_currency} already exists.'
                )
            except cryptonator.CryptonatorException as e:
                raise serializers.ValidationError({"details": str(e)})
            else:
                return instance
        raise serializers.ValidationError({"details": "currency didn't match with correct name"})
