## 1. Install requirements


Local requirements:

    pip install -r requirements.txt


## 2. Django Management commands

* ``./manage.py migrate``
* ``./manage.py collectstatic``
* ``./manage.py createsuperuser``


## 3. Celery worker:

* ``celery worker -A apps.taskapp -E -l INFO -Q normal -n normal``

## 5. Celery beat:

* ``celery beat -A apps.taskapp -l INFO``

## 6. runserver:
   * ``python manage.py runserver 0.0.0.0:8000``